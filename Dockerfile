FROM python:3-slim as builder

RUN groupadd --gid 1000 python \
&& useradd --uid 1000 --gid python --shell /bin/bash --create-home python
ENV PYROOT /home/python/.local
ENV PYTHONUSERBASE $PYROOT

USER python
WORKDIR /home/python
COPY requirements.txt /home/python
RUN pip install  --user --no-cache-dir -r requirements.txt

FROM python:3-slim
RUN groupadd --gid 1000 python \
&& useradd --uid 1000 --gid python --shell /bin/bash --create-home python
ENV PYROOT /home/python/.local
ENV PYTHONUSERBASE $PYROOT

LABEL maintainer="Abel Luck <abel@guardianproject.info>"
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="digiresilience/zammad_exporter"
LABEL org.label-schema.description="Zammad metrics exporter for Prometheus"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.vcs-url=$VCS_URL
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.version=$VERSION
USER python
WORKDIR /home/python
COPY --chown=python --from=builder $PYROOT/lib/ $PYROOT/lib/
COPY zammad_exporter.py /home/python/
EXPOSE 9118
ENV ZAMMAD_HOST=http://zammad:80
ENV ZAMMAD_EXPORTER_PORT=9390
ENTRYPOINT [ "python", "-u", "/home/python/zammad_exporter.py" ]
