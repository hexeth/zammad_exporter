# HELP zammad_collector_collect_seconds Time spent to collect metrics from zammad
# TYPE zammad_collector_collect_seconds summary
zammad_collector_collect_seconds_count 18263.0
zammad_collector_collect_seconds_sum 0.44335165308439173
# TYPE zammad_collector_collect_seconds_created gauge
zammad_collector_collect_seconds_created 1.5484119514847836e+09
# HELP zammad_agent_count Total number of agents
# TYPE zammad_agent_count gauge
zammad_agent_count 1.0
# HELP zammad_user_count Total number of users
# TYPE zammad_user_count gauge
zammad_user_count 3.0
# HELP zammad_group_count Total number of groups
# TYPE zammad_group_count gauge
zammad_group_count 1.0
# HELP zammad_overview_count Total number of overviews
# TYPE zammad_overview_count gauge
zammad_overview_count 9.0
# HELP zammad_ticket_count Total number of tickets
# TYPE zammad_ticket_count gauge
zammad_ticket_count 1.0
# HELP zammad_ticket_article_count Total number of ticket_articles
# TYPE zammad_ticket_article_count gauge
zammad_ticket_article_count 1.0
# HELP zammad_text_module_count Total number of text_modules
# TYPE zammad_text_module_count gauge
zammad_text_module_count 27.0
# HELP zammad_storage_bytes Total bytes consumed by zammad file storage
# TYPE zammad_storage_bytes gauge
zammad_storage_bytes 55296.0
# HELP zammad_last_login_seconds Seconds since the last login
# TYPE zammad_last_login_seconds gauge
zammad_last_login_seconds 7293.0
# HELP zammad_last_created_user_seconds Seconds since last user was created
# TYPE zammad_last_created_user_seconds gauge
zammad_last_created_user_seconds 7299.0
# HELP zammad_last_created_group_seconds Seconds since last group was created
# TYPE zammad_last_created_group_seconds gauge
zammad_last_created_group_seconds 80777.0
# HELP zammad_last_created_overview_seconds Seconds since last overview was created
# TYPE zammad_last_created_overview_seconds gauge
zammad_last_created_overview_seconds 80775.0
# HELP zammad_last_created_ticket_seconds Seconds since last ticket was created
# TYPE zammad_last_created_ticket_seconds gauge
zammad_last_created_ticket_seconds 80776.0
# HELP zammad_last_created_ticket_article_seconds Seconds since last ticket_article was created
# TYPE zammad_last_created_ticket_article_seconds gauge
zammad_last_created_ticket_article_seconds 80776.0
# HELP zammad_last_created_text_module_seconds Seconds since last text_module was created
# TYPE zammad_last_created_text_module_seconds gauge
zammad_last_created_text_module_seconds 7294.0
# HELP zammad_healthy The monitoring status reported by zammad
# TYPE zammad_healthy gauge
zammad_healthy{zammad_healthy="healthy"} 1.0

