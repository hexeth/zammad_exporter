#!/usr/bin/env python3
__version__ = "0.3"

import argparse
import time
import os
import json
import requests
from sys import exit
from datetime import datetime
from prometheus_client import start_http_server, Summary
from prometheus_client.core import (
    GaugeMetricFamily,
    StateSetMetricFamily,
)
from prometheus_client import REGISTRY


def gauge(name, description, json, value):
    v = json[value]
    if v is None:
        v = -1
    return GaugeMetricFamily("zammad_%s" % (name), description, value=v)


def seconds_gauge(name, description, json, value):
    v = date_to_seconds(json[value])
    if v is None:
        v = -1
    return GaugeMetricFamily("zammad_%s" % (name), description, value=v)


def date_to_seconds(d, now=datetime.now()):
    """
    d must be a RFC 3339 string IN ZULU time
    ref: https://stackoverflow.com/a/30696682
    """
    if d is None:
        return None
    dt = datetime.strptime(d, "%Y-%m-%dT%H:%M:%S.%fZ")
    diff = now - dt
    return int(diff.total_seconds())


COLLECTION_TIME = Summary(
    "zammad_collector_collect_seconds", "Time spent to collect metrics from zammad"
)


class ZammadCollector(object):
    def __init__(self, target, token):
        self._target = target
        self._token = token
        self._url_health = "%s/api/v1/monitoring/health_check?token=%s" % (
            self._target,
            self._token,
        )
        self._url_status = "%s/api/v1/monitoring/status?token=%s" % (
            self._target,
            self._token,
        )

    def unavailable(self):
        return StateSetMetricFamily(
            "zammad_healthy",
            "The monitoring status reported by zammad",
            value={"healthy": False},
        )

    @COLLECTION_TIME.time()
    def collect(self):
        try:
            r = requests.get(self._url_health)
            r.raise_for_status()
            health_response = r.json()

            r = requests.get(self._url_status)
            r.raise_for_status()
            status_response = r.json()
        except json.decoder.JSONDecodeError as e:
            print("JSON decode error", e)
            return self.unavailable()
        except requests.exceptions.RequestException as e:
            print("HTTP Error", e)
            return self.unavailable()

        counts = status_response["counts"]

        yield gauge("agent_count", "Total number of agents", status_response, "agents")
        yield gauge("user_count", "Total number of users", counts, "users")
        yield gauge("group_count", "Total number of groups", counts, "groups")
        yield gauge("overview_count", "Total number of overviews", counts, "overviews")
        yield gauge("ticket_count", "Total number of tickets", counts, "tickets")
        yield gauge(
            "ticket_article_count",
            "Total number of ticket_articles",
            counts,
            "ticket_articles",
        )
        yield gauge(
            "text_module_count", "Total number of text_modules", counts, "text_modules"
        )

        if "storage" in status_response:
            yield GaugeMetricFamily(
                "zammad_storage_bytes",
                "Total bytes consumed by zammad file storage",
                value=int(status_response["storage"]["kB"]) * 1024,
            )

        last_created_at = status_response["last_created_at"]
        yield seconds_gauge(
            "last_login_seconds",
            "Seconds since the last login",
            status_response,
            "last_login",
        )
        yield seconds_gauge(
            "last_created_user_seconds",
            "Seconds since last user was created",
            last_created_at,
            "users",
        )
        yield seconds_gauge(
            "last_created_group_seconds",
            "Seconds since last group was created",
            last_created_at,
            "groups",
        )
        yield seconds_gauge(
            "last_created_overview_seconds",
            "Seconds since last overview was created",
            last_created_at,
            "overviews",
        )
        yield seconds_gauge(
            "last_created_ticket_seconds",
            "Seconds since last ticket was created",
            last_created_at,
            "tickets",
        )
        yield seconds_gauge(
            "last_created_ticket_article_seconds",
            "Seconds since last ticket_article was created",
            last_created_at,
            "ticket_articles",
        )
        yield seconds_gauge(
            "last_created_text_module_seconds",
            "Seconds since last text_module was created",
            last_created_at,
            "text_modules",
        )

        is_healthy = health_response["healthy"]
        yield StateSetMetricFamily(
            "zammad_healthy",
            "The monitoring status reported by zammad",
            value={"healthy": is_healthy},
        )

        health_message = health_response["message"]
        yield StateSetMetricFamily(
            "zammad_health_message",
            "The health message reported by zammad",
            value={health_message: "message"},
        )


def cli():
    parser = argparse.ArgumentParser(description="Zammad exporter for Prometheus")
    parser.add_argument(
        "--zammad-host",
        "-z",
        help="The zammad host, PROTO:HOST:PORT. " + "(default http://127.0.0.1:80)",
        default=os.environ.get("ZAMMAD_HOST", "http://127.0.0.1:80"),
    )

    parser.add_argument(
        "--bind-address",
        "-b",
        dest="bind_address",
        help="The address to which the exporter should bind (default: 0.0.0.0)",
        default=os.environ.get("ZAMMAD_EXPORTER_HOST", "0.0.0.0"),
    )
    parser.add_argument(
        "--bind-port",
        "-p",
        dest="bind_port",
        type=int,
        help="Port the exporter should listen on. (default 9390)",
        default=int(os.environ.get("ZAMMAD_EXPORTER_PORT", "9390")),
    )
    parser.add_argument(
        "-v",
        "--version",
        dest="version",
        action="store_true",
        help="Display version information and exit",
    )
    token_group = parser.add_mutually_exclusive_group()
    token_group.add_argument(
        "--token",
        dest="token",
        help="Zammad monitoring token.",
        default=os.environ.get("ZAMMAD_TOKEN"),
    )
    token_group.add_argument(
        "--token-file",
        dest="token_file",
        help="File containing the Zammad monitoring token",
        default=os.environ.get("ZAMMAD_TOKEN_FILE"),
    )
    return parser


def get_token(token, token_file):
    if token_file is None:
        return token
    else:
        try:
            with open(token_file) as fd:
                return fd.read().strip()
        except OSError as err:
            exit("Failed to open file {0}.\n{1}".format(token_file, err))
        except UnicodeError as err:
            exit("Failed to read file {0}.\n{1}".format(token_file, err))


def check_access(host, token):
    url = "{}/api/v1/monitoring/health_check?token={}".format(host, token)
    try:
        r = requests.get(url)
        if r.status_code != 200:
            print(r)
            print("Non-200 response from Zammad")
            return False
    except requests.exceptions.RequestException as e:
        print(e)
        return False
    return True


def verify_access(host, token):
    """Blocks until exporter is able to access Zammad"""
    success = False
    while not success:
        success = check_access(host, token)
        if not success:
            print("Retrying connection to {}".format(host))
        time.sleep(1)
    print("Connected to Zammad at {}".format(host))


def parse_args():
    parser = cli()
    args = parser.parse_args()
    # display version and exit
    if args.version is True:
        print("Version is {0}".format(__version__))
        exit(0)
    # check if password has been set
    if args.token is None and args.token_file is None:
        parser.error("Option --token or --token-file must be set.")
    return args


def start(host, port, zammad_host, token):
    start_http_server(port, host)
    REGISTRY.register(ZammadCollector(zammad_host, token))
    print("Polling {}. Serving at port: {}".format(host, port))


def main():
    args = parse_args()

    token = get_token(args.token, args.token_file)

    # Verify Zammad is reachable
    verify_access(args.zammad_host, token)

    start(args.bind_address, args.bind_port, args.zammad_host, token)
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        print("Interrupted")
        exit(0)


if __name__ == "__main__":
    main()
