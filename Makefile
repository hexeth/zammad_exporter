export IMAGE_NAME ?= $(CI_PROJECT_PATH)
export TARGET_DOCKER_REGISTRY ?= registry.gitlab.com
export SOURCE_DOCKER_REGISTRY ?= $(TARGET_DOCKER_REGISTRY)
export TARGET_VERSION ?= tests
export DOCKER_IMAGE_NAME ?= $(TARGET_DOCKER_REGISTRY)/$(IMAGE_NAME):$(TARGET_VERSION)
README_DEPS = docs/cli.md
TEST_SRC = test.py
-include $(shell curl -sSL -o .build-harness "https://gitlab.com/snippets/1957473/raw"; echo .build-harness)

docs/cli.md:
	@( \
		echo "### CLI"; \
		echo '```'; \
		$(SOURCE_VENV) ;\
		python zammad_exporter.py --help; \
		echo '```'; \
	) > $@

docker/test:
	docker run --rm --name image-under-test $(DOCKER_IMAGE_NAME) --help
